﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;


/// <summary>
/// 
/// </summary>
/// 
public static partial class TN_Utils
{

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Input"></param>
    /// <returns></returns>
    public static string Create64String(Stream Input)
    {
        Stream fs = Input;
        BinaryReader br = new BinaryReader(fs);
        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
        string base64String = "data:image/png;base64," + Convert.ToBase64String(bytes, 0, bytes.Length);
        return base64String;
    }

    #region [Random Number]
    // Instantiate random number generator.  
    // It is better to keep a single Random instance 
    // and keep using Next on the same instance.  
    private static readonly Random _random = new Random();

    // Generates a random number within a range.      
    public static int RandomNumber(int min, int max)
    {
        return _random.Next(min, max);
    }

    // Generates a random string with a given size.    
    public static string RandomString(int size, bool lowerCase = false)
    {
        var builder = new StringBuilder(size);

        // Unicode/ASCII Letters are divided into two blocks
        // (Letters 65–90 / 97–122):   
        // The first group containing the uppercase letters and
        // the second group containing the lowercase.  

        // char is a single Unicode character  
        char offset = lowerCase ? 'a' : 'A';
        const int lettersOffset = 26; // A...Z or a..z: length = 26  

        for (var i = 0; i < size; i++)
        {
            var @char = (char)_random.Next(offset, offset + lettersOffset);
            builder.Append(@char);
        }

        return lowerCase ? builder.ToString().ToLower() : builder.ToString();
    }

    // Generates a random password.  
    // 4-LowerCase + 4-Digits + 2-UpperCase  
    public static string RandomPassword()
    {
        var passwordBuilder = new StringBuilder();

        // 4-Letters lower case   
        passwordBuilder.Append(RandomString(4, true));

        // 4-Digits between 1000 and 9999  
        passwordBuilder.Append(RandomNumber(1000, 9999));

        // 2-Letters upper case  
        passwordBuilder.Append(RandomString(2));
        return passwordBuilder.ToString();
    }
    #endregion

    #region [QR CODE]
    public static string QRScan(string path, out string Message)
    {
        string barcode = "";
        try
        {
            var barcodeReader = new BarcodeReader();
            barcodeReader.AutoRotate = true;
            barcodeReader.TryInverted = true;
            var barcodeBitmap = (Bitmap)Bitmap.FromFile(path);
            barcode = barcodeReader.Decode(barcodeBitmap).Text;
            barcodeBitmap.Dispose();
            Message = string.Empty;
        }
        catch (Exception ex)
        {
            Message = ex.ToString();
        }
        return barcode;
    }
    public static string QRCode(string content, int Width, int Height, out string Message)
    {
        string b64string = "";
        try
        {
            var options = new QrCodeEncodingOptions
            {
                DisableECI = true,
                CharacterSet = "UTF-8",
                Width = Width,
                Height = Height,
            };

            var barcodeWrite = new BarcodeWriter();
            barcodeWrite.Format = BarcodeFormat.QR_CODE;
            barcodeWrite.Options = options;

            var bitmap = new Bitmap(barcodeWrite.Write(content));

            byte[] array = (byte[])new ImageConverter().ConvertTo(bitmap, typeof(byte[]));
            b64string = String.Format("data:image/png;base64,{0}", Convert.ToBase64String(array));
            Message = string.Empty;
        }
        catch (Exception ex)
        {
            Message = ex.ToString();
        }
        return b64string;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <param name="height"></param>
    /// <param name="width"></param>
    /// <param name="showtitle">Hiển thị string ngay dưới mã barcode</param>
    /// <param name="Message"></param>
    /// <returns></returns>
    public static string BarCode(string input, int height, int width, bool showtitle, out string Message)
    {
        string b64string = "";
        try
        {
            BarcodeWriter writer = new BarcodeWriter()
            {
                Format = BarcodeFormat.CODE_128,
                Options = new EncodingOptions
                {
                    Height = height,
                    Width = width,
                    PureBarcode = showtitle,
                    Margin = 10,
                },
            };

            var bitmap = writer.Write(input);

            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Png);
            byte[] array = ms.ToArray();
            ms.Dispose();
            bitmap.Dispose();
            b64string = String.Format("data:image/png;base64,{0}", Convert.ToBase64String(array));
            Message = string.Empty;
        }
        catch (Exception ex)
        {
            Message = ex.ToString();
        }
        return b64string;
    }
    #endregion

    #region [DataTable Helper]
    /// <summary>
    /// Convert Excel to c# table
    /// </summary>
    /// <param name="sheetName"></param>
    /// <param name="path"></param>
    /// <returns></returns>
    public static DataTable ExcelToCSharp(string sheetName, string path)
    {
        using (OleDbConnection conn = new OleDbConnection())
        {
            DataTable dt = new DataTable();
            string Import_FileName = path;
            string fileExtension = Path.GetExtension(Import_FileName);
            if (fileExtension == ".xls")
            {
                conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 8.0;HDR=YES;'";
            }

            if (fileExtension == ".xlsx")
            {
                conn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 12.0 Xml;HDR=YES;'";
            }

            using (OleDbCommand comm = new OleDbCommand())
            {
                comm.CommandText = "SELECT * FROM [" + sheetName + "$]";
                comm.Connection = conn;
                using (OleDbDataAdapter da = new OleDbDataAdapter())
                {
                    da.SelectCommand = comm;
                    da.Fill(dt);
                    return dt;
                }
            }
        }
    }
    /// <summary>
    /// Export DataTable to Excel file
    /// </summary>
    /// <param name="DataTable">Source DataTable</param>
    /// <param name="ExcelFilePath">Path to result file name</param>
    public static void ToExcel(this DataTable DataTable, string ExcelFilePath = null)
    {
        try
        {
            int ColumnsCount;

            if (DataTable == null || (ColumnsCount = DataTable.Columns.Count) == 0)
            {
                throw new Exception("ExportToExcel: Null or empty input table!\n");
            }

            // load excel, and create a new workbook
            Microsoft.Office.Interop.Excel.Application Excel = new Microsoft.Office.Interop.Excel.Application();
            Excel.Workbooks.Add();

            // single worksheet
            Microsoft.Office.Interop.Excel._Worksheet Worksheet = Excel.ActiveSheet;

            object[] Header = new object[ColumnsCount];

            // column headings               
            for (int i = 0; i < ColumnsCount; i++)
            {
                Header[i] = DataTable.Columns[i].ColumnName;
            }

            Microsoft.Office.Interop.Excel.Range HeaderRange = Worksheet.get_Range((Microsoft.Office.Interop.Excel.Range)(Worksheet.Cells[1, 1]), (Microsoft.Office.Interop.Excel.Range)(Worksheet.Cells[1, ColumnsCount]));
            HeaderRange.Value = Header;
            HeaderRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray);
            HeaderRange.Font.Bold = true;

            // DataCells
            int RowsCount = DataTable.Rows.Count;
            object[,] Cells = new object[RowsCount, ColumnsCount];

            for (int j = 0; j < RowsCount; j++)
            {
                for (int i = 0; i < ColumnsCount; i++)
                {
                    Cells[j, i] = DataTable.Rows[j][i];
                }
            }

            Worksheet.get_Range((Microsoft.Office.Interop.Excel.Range)(Worksheet.Cells[2, 1]), (Microsoft.Office.Interop.Excel.Range)(Worksheet.Cells[RowsCount + 1, ColumnsCount])).Value = Cells;

            // check fielpath
            if (ExcelFilePath != null && ExcelFilePath != "")
            {
                try
                {
                    Worksheet.SaveAs(ExcelFilePath);
                    Excel.Quit();
                    Process.Start(ExcelFilePath);
                    //System.Windows.MessageBox.Show("Excel file saved!");
                }
                catch (Exception ex)
                {
                    throw new Exception("ExportToExcel: Excel file could not be saved! Check filepath.\n"
                        + ex.Message);
                }
            }
            else    // no filepath is given
            {
                Excel.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("ExportToExcel: \n" + ex.Message);
        }
    }
    #endregion

    #region [String Helper]    

    /// <summary>
    /// Trim tất cả các khoản trắng dư thừa đầu, cuối, và giữa các chuỗi, nếu null thì trả empty string
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string ExTrim(this string input)
    {
        if (string.IsNullOrEmpty(input))
            return string.Empty;
        else
            return ConvertWhitespacesToSingleSpaces(input);
    }

    public static string ConvertWhitespacesToSingleSpaces(this string value)
    {
        return Regex.Replace(value, @"\s+", " ");
    }

    /// <summary>
    /// trả về ngày đầu tuần của ngày chỉ định
    /// </summary>
    /// <param name="date"></param>
    /// <returns></returns>
    public static DateTime FirstDayOfWeek(DateTime date)
    {
        DayOfWeek fdow = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
        int offset = fdow - date.DayOfWeek;
        DateTime fdowDate = date.AddDays(offset);
        return fdowDate;
    }
    /// <summary>
    /// trả về ngày cuối tuấn của ngày chỉ định
    /// </summary>
    /// <param name="date"></param>
    /// <returns></returns>
    public static DateTime LastDayOfWeek(DateTime date)
    {
        DateTime ldowDate = FirstDayOfWeek(date).AddDays(6);
        return ldowDate;
    }

    /// <summary>Trim all String properties of the given object</summary>
    public static TSelf TrimStringProperties<TSelf>(this TSelf input)
    {
        if (input == null)
        {
            return input;
        }

        var stringProperties = typeof(TSelf).GetProperties()
            .Where(p => p.PropertyType == typeof(string));

        foreach (var stringProperty in stringProperties)
        {
            string currentValue = (string)stringProperty.GetValue(input, null);
            if (currentValue != null)
            {
                stringProperty.SetValue(input, currentValue.Trim(), null);
            }
        }
        return input;
    }

    /// <summary>
    /// Lấy dòng đầu tiên của chuỗi, khi chuỗi có nhiều xuống dòng
    /// </summary>
    /// <param name="Input"></param>
    /// <returns></returns>
    public static string GetFirstLine(this string Input)
    {
        return new StringReader(Input).ReadLine();
    }

    /// <summary>
    /// Kiem tra phải là GUID hay không
    /// </summary>
    /// <param name="inputString"></param>
    /// <returns></returns>
    public static bool ValidateGuid(string inputString)
    {
        Guid guidOutput;
        return Guid.TryParse(inputString, out guidOutput);
    }
    /// <summary>
    /// Cắt chuỗi nhiều ký tự ví dụ: nhập vào [I'll text you my address 780 Trần Hưng Đạo]
    /// </summary>
    /// <param name="strContent">chuỗi</param>
    /// <param name="Length">độ dài cần lấy</param>
    /// <returns>I'll text you my address 780 ....</returns>
    public static string GetShortContent(string strContent, int Length)
    {
        if (strContent.Length < Length)
        {
            return strContent;
        }
        else
        {
            return (strContent.Substring(0, Length) + "...");
        }
    }

    /// <summary>
    /// Chữ hoa đầu dòng
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string FirstCharToUpper(this string input)
    {
        if (String.IsNullOrEmpty(input))
        {
            return string.Empty;
        }

        return input.First().ToString().ToUpper() + input.Substring(1);
    }

    /// <summary>
    /// Chuyển số sang chữ ENGLISH
    /// </summary>
    /// <param name="lNumber"></param>
    /// <returns></returns>
    public static string NumberToWordsEN(long lNumber)
    {

        string[] ones = {"One ","Two ","Three ","Four ","Five ","Six ","Seven ","Eight ","Nine ","Ten ",
                              "Eleven ","Twelve ","Thirteen ","Fourteen ","Fifteen ","Sixteen ","Seventeen ","Eighteen ","Ninteen "
                            };
        string[] tens = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninty " };

        if (lNumber == 0)
        {
            return ("");
        }

        if (lNumber < 0)
        {

            lNumber *= -1;
        }
        if (lNumber < 20)
        {
            return ones[lNumber - 1];
        }
        if (lNumber <= 99)
        {
            return tens[(lNumber / 10) - 2] + NumberToWordsEN(lNumber % 10);
        }
        if (lNumber < 1000)
        {
            return NumberToWordsEN(lNumber / 100) + "Hundred " + NumberToWordsEN(lNumber % 100);
        }
        if (lNumber < 100000)
        {
            return NumberToWordsEN(lNumber / 1000) + "Thousand " + NumberToWordsEN(lNumber % 1000);
        }
        if (lNumber < 10000000)
        {
            return NumberToWordsEN(lNumber / 100000) + "Lakh " + NumberToWordsEN(lNumber % 100000);
        }
        if (lNumber < 1000000000)
        {
            return NumberToWordsEN(lNumber / 10000000) + "Crore " + NumberToWordsEN(lNumber % 10000000);
        }
        return "";
    }

    /// <summary>
    /// Chuyển số sang chữ VN
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public static string NumberToWordsVN(double number)
    {
        string s = number.ToString("#");
        string[] so = new string[] { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
        string[] hang = new string[] { "", "nghìn", "triệu", "tỷ" };
        int i, j, donvi, chuc, tram;
        string str = " ";
        bool booAm = false;
        decimal decS = 0;
        //Tung addnew
        try
        {
            decS = Convert.ToDecimal(s.ToString());
        }
        catch
        {
        }
        if (decS < 0)
        {
            decS = -decS;
            s = decS.ToString();
            booAm = true;
        }
        i = s.Length;
        if (i == 0)
        {
            str = so[0] + str;
        }
        else
        {
            j = 0;
            while (i > 0)
            {
                donvi = Convert.ToInt32(s.Substring(i - 1, 1));
                i--;
                if (i > 0)
                {
                    chuc = Convert.ToInt32(s.Substring(i - 1, 1));
                }
                else
                {
                    chuc = -1;
                }

                i--;
                if (i > 0)
                {
                    tram = Convert.ToInt32(s.Substring(i - 1, 1));
                }
                else
                {
                    tram = -1;
                }

                i--;
                if ((donvi > 0) || (chuc > 0) || (tram > 0) || (j == 3))
                {
                    str = hang[j] + str;
                }

                j++;
                if (j > 3)
                {
                    j = 1;
                }

                if ((donvi == 1) && (chuc > 1))
                {
                    str = "một " + str;
                }
                else
                {
                    if ((donvi == 5) && (chuc > 0))
                    {
                        str = "lăm " + str;
                    }
                    else if (donvi > 0)
                    {
                        str = so[donvi] + " " + str;
                    }
                }
                if (chuc < 0)
                {
                    break;
                }
                else
                {
                    if ((chuc == 0) && (donvi > 0))
                    {
                        str = "lẻ " + str;
                    }

                    if (chuc == 1)
                    {
                        str = "mười " + str;
                    }

                    if (chuc > 1)
                    {
                        str = so[chuc] + " mươi " + str;
                    }
                }
                if (tram < 0)
                {
                    break;
                }
                else
                {
                    if ((tram > 0) || (chuc > 0) || (donvi > 0))
                    {
                        str = so[tram] + " trăm " + str;
                    }
                }
                str = " " + str;
            }
        }
        if (booAm)
        {
            str = "Âm " + str;
        }

        return str.Trim();
    }

    /// <summary>
    /// Xóa HTML tag từ chuỗi
    /// </summary>
    /// <param name="Txt"></param>
    /// <returns></returns>
    public static string StripHtml(this string Txt)
    {
        if (Txt != null)
        {
            return Regex.Replace(Txt, "<(.|\\n)*?&#>", string.Empty);
        }
        else
        {
            return "";
        }
    }
    #endregion

    #region [Object to Number]
    public static int ToInt(this object obj)
    {
        try
        {
            return int.Parse(obj.ToString());
        }
        catch
        {
            return 0;
        }
    }
    public static float ToFloat(this object obj)
    {
        float zResult = 0;
        if(obj==null ||obj.ToString()=="")
        {
            return 0;
        }   
        else
        {
            if (float.TryParse(obj.ToString(), out zResult))
            {

            }
            return zResult;
        }    
    }
    public static decimal ToDecimal(this object obj)
    {
        decimal zResult = 0;
        if (obj == null || obj.ToString() == "")
        {
            return 0;
        }
        else
        {
            if (decimal.TryParse(obj.ToString(), out zResult))
            {

            }
            return zResult;
        }
    }
    public static double ToRound(this object obj)
    {
        double zResult = 0;
        if (obj == null || obj.ToString() == "")
        {
            return 0;
        }
        else
        {
            if (double.TryParse(obj.ToString(), out zResult))
            {

            }
            return Math.Round(zResult);
        }
    }
    public static double ToDouble(this object obj)
    {
        double zResult = 0;
        if (obj == null || obj.ToString() == "")
        {
            return 0;
        }
        else
        {
            if (double.TryParse(obj.ToString(), out zResult))
            {

            }
            return zResult;
        }
    }
    #endregion

    #region [Object & JSON] 
    /// <summary>
    /// sao chép giá trị của đối tượng cùng kiểu
    /// </summary>
    /// <typeparam name="T">object source</typeparam>
    /// <typeparam name="TU">object </typeparam>
    /// <param name="source"></param>
    /// <param name="dest"></param>
    public static void CopyPropertiesTo<T, TU>(this T source, TU dest)
    {
        var sourceProps = typeof(T).GetProperties().Where(x => x.CanRead).ToList();
        var destProps = typeof(TU).GetProperties()
                .Where(x => x.CanWrite)
                .ToList();

        foreach (var sourceProp in sourceProps)
        {
            if (destProps.Any(x => x.Name == sourceProp.Name))
            {
                var p = destProps.First(x => x.Name == sourceProp.Name);
                if (p.CanWrite)
                { // check if the property can be set or no.
                    Type t = p.PropertyType;
                    switch (Type.GetTypeCode(t))
                    {
                        case TypeCode.Double:
                            double zDoubleVal = sourceProp.GetValue(source, null).ToDouble();
                            p.SetValue(dest, zDoubleVal, null);
                            break;

                        case TypeCode.Int32:
                            int zIntVal = sourceProp.GetValue(source, null).ToInt();
                            p.SetValue(dest, zIntVal, null);
                            break;

                        case TypeCode.DateTime:
                            string zVal = sourceProp.GetValue(source, null).ToString();
                            DateTime zDateVal = DateTime.MinValue;

                            try
                            {
                                DateTime.TryParseExact(sourceProp.GetValue(source, null).ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDateVal);
                            }
                            catch (Exception)
                            {
                                DateTime.TryParseExact(sourceProp.GetValue(source, null).ToString(), "dd/MM/yyyy HH:mm:tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out zDateVal);
                            }

                            p.SetValue(dest, zDateVal, null);
                            break;

                        case TypeCode.Decimal:
                            double zDecimal;
                            double.TryParse(sourceProp.GetValue(source, null).ToString(), out zDecimal);
                            p.SetValue(dest, zDecimal, null);
                            break;

                        default:
                            p.SetValue(dest, sourceProp.GetValue(source, null).ToString(), null);
                            break;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Kiểm tra từng giá trị trong 2 đối tượng cùng kiểu dữ liệu
    /// </summary>
    /// <param name="obj">obj 1</param>
    /// <param name="another">obj 2 </param>
    /// <returns>true, false</returns>
    public static bool CompareObject(object obj, object another)
    {
        if (ReferenceEquals(obj, another))
        {
            return true;
        }

        if ((obj == null) || (another == null))
        {
            return false;
        }
        //Compare two object's class, return false if they are difference
        if (obj.GetType() != another.GetType())
        {
            return false;
        }

        var result = true;
        //Get all properties of obj
        //And compare each other
        foreach (var property in obj.GetType().GetProperties())
        {
            var objValue = property.GetValue(obj);
            var anotherValue = property.GetValue(another);
            if (!objValue.Equals(anotherValue))
            {
                result = false;
                break;
            }
        }

        return result;
    }

    /// <summary>
    /// Kiểm tra từng giá trị trong 2 đối tượng cùng kiểu dữ liệu
    /// </summary>
    /// <param name="obj">This OBJECT</param>
    /// <param name="another">New OBJECT</param>
    /// <returns>New Object</returns>
    public static object CopyObject(this object obj, object another, out string Message)
    {
        //Compare two object's class, return false if they are difference
        if (obj.GetType() != another.GetType())
        {
            Message = "2 object must be the same";
        }

        //Get all properties of obj
        //And compare each other
        foreach (var property in obj.GetType().GetProperties())
        {
            var objValue = property.GetValue(obj);
            var anotherValue = property.GetValue(another);
            if (objValue != null &&
                objValue.ToString() != string.Empty)
            {
                anotherValue = objValue;
                //result = false;
                //break;
            }
        }
        Message = "OK";
        return another;
    }

    #endregion

    #region [Function Call WebService]
    //public static async Task<ApiConnect> GetRequest(string Url, string Method, Dictionary<string, string> Param)
    //{
    //    ApiConnect apiResult = new ApiConnect();
    //    string queryString = string.Empty;
    //    using (var content = new FormUrlEncodedContent(Param))
    //    {
    //        queryString = content.ReadAsStringAsync().Result;
    //    }


    //    using (var client = new HttpClient())
    //    {
    //        client.BaseAddress = new Uri(Url);  //Uri("http://localhost:55587/");
    //        client.DefaultRequestHeaders.Accept.Clear();
    //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    //        //GET Method  
    //        HttpResponseMessage response = await client.GetAsync(Method);   //("api/Department/1");
    //        if (response.IsSuccessStatusCode)
    //        {
    //            var result = await response.Content.ReadAsStringAsync();

    //            apiResult.Message = "SUCCESS";
    //            apiResult.Status = 200;
    //            apiResult.Data = result;

    //        }
    //        else
    //        {
    //            apiResult.Message = "Internal server Error";
    //            apiResult.Status = 500;
    //            apiResult.Data = "";
    //        }
    //    }

    //    return apiResult;
    //}

    ///// <summary>
    ///// Request Action From Webservice, cách gọi: Object X = Task.Run(async () => await TN_Utils.AccessWebService_Async(URL, Method, Param)).Result;
    ///// </summary>
    ///// <param name="url">Đường dẫn</param>
    ///// <param name="method">Phương thức</param>
    ///// <param name="Param">Các tham số</param>
    ///// <returns>Trả về kiểu đối tượng X</returns>
    //public static async Task<ApiConnect> AccessWebService_Async(string url, string method, Dictionary<string, string> Param)
    //{
    //    ApiConnect apiResult = new ApiConnect();
    //    try
    //    {
    //        byte[] array = CreateHttpRequestData(Param);
    //        HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url + "/" + method);
    //        httpWebRequest.Method = "POST";
    //        httpWebRequest.KeepAlive = false;
    //        httpWebRequest.ContentType = "application/x-www-form-urlencoded";
    //        httpWebRequest.ContentLength = array.Length;
    //        httpWebRequest.Timeout = 30000;
    //        string data = string.Empty;
    //        using (var newStream = httpWebRequest.GetRequestStream())
    //        {
    //            newStream.Write(array, 0, array.Length);
    //        }
    //        HttpWebResponse httpWebResponse = (HttpWebResponse)(await httpWebRequest.GetResponseAsync());
    //        Stream responseStream = httpWebResponse.GetResponseStream();
    //        StreamReader streamReader = new StreamReader(responseStream);

    //        data = streamReader.ReadToEnd();
    //        streamReader.Close();
    //        responseStream.Close();
    //        apiResult.Message = "SUCCESS";
    //        apiResult.Status = 200;
    //        apiResult.Data = data;
    //    }
    //    catch (Exception ex)
    //    {
    //        apiResult.Status = 500;
    //        apiResult.Message = new StringReader(ex.ToString()).ReadLine();
    //        apiResult.Data = "";
    //    }
    //    return apiResult;
    //}
    ///// <summary>
    ///// Request Action From Webservice
    ///// </summary>
    ///// <param name="url">Đường dẫn</param>
    ///// <param name="method">Phương thức</param>
    ///// <param name="Param">Các tham số</param>
    ///// <returns>ApiResult X</returns>
    //public static ApiConnect AccessWebService_Sync(string url, string method, Dictionary<string, string> Param)
    //{
    //    ApiConnect apiResult = new ApiConnect();
    //    try
    //    {
    //        byte[] array = CreateHttpRequestData(Param);
    //        HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url + "/" + method);
    //        httpWebRequest.Method = "POST";
    //        httpWebRequest.KeepAlive = false;
    //        httpWebRequest.ContentType = "application/x-www-form-urlencoded";
    //        httpWebRequest.ContentLength = array.Length;
    //        httpWebRequest.Timeout = 30000;
    //        string data = string.Empty;

    //        using (var newStream = httpWebRequest.GetRequestStream())
    //        {
    //            newStream.Write(array, 0, array.Length);
    //        }
    //        HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
    //        Stream responseStream = httpWebResponse.GetResponseStream();
    //        StreamReader streamReader = new StreamReader(responseStream);
    //        data = streamReader.ReadToEnd();
    //        streamReader.Close();
    //        responseStream.Close();
    //        apiResult.Message = "SUCCESS";
    //        apiResult.Status = 200;
    //        apiResult.Data = data;
    //    }
    //    catch (Exception ex)
    //    {
    //        apiResult.Status = 500;
    //        apiResult.Message = new StringReader(ex.ToString()).ReadLine();
    //        apiResult.Data = "";
    //    }
    //    return apiResult;
    //}

    //private static byte[] CreateHttpRequestData(Dictionary<string, string> Param)
    //{
    //    StringBuilder stringBuilder = new StringBuilder();
    //    foreach (string current in Param.Keys)
    //    {
    //        stringBuilder.Append(current);
    //        stringBuilder.Append('=');
    //        stringBuilder.Append(Param[current]);
    //        stringBuilder.Append('&');
    //    }
    //    stringBuilder.Remove(stringBuilder.Length - 1, 1);
    //    UTF8Encoding uTF8Encoding = new UTF8Encoding();
    //    return uTF8Encoding.GetBytes(stringBuilder.ToString());
    //}

    #endregion

    #region [Chức năng khác]
    /// <summary>
    /// Đổi time từ kiểu Unix number to datetime c#
    /// </summary>
    /// <param name="unixTimeStamp"></param>
    /// <returns></returns>
    public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
    {
        // Unix timestamp is seconds past epoch
        try
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dateTime;
        }
        catch (Exception)
        {
            return DateTime.MinValue;
        }
    }
    public static string FormatMoneyFromString(this string input)
    {
        try
        {
            if (input.ToString() != "0")
            {
                double zResult = double.Parse(input.ToString(), CultureInfo.GetCultureInfo("vi-VN"));

                string n = "";
                if (zResult % 1 == 0)
                {
                    n = "n0";
                }
                else
                {
                    n = "n2";
                }

                return zResult.ToString(n, CultureInfo.GetCultureInfo("vi-VN"));
            }
            else
            {
                return "0";
            }
        }
        catch (Exception)
        {
            return "Lỗi số";
        }
    }
    public static string FormatMoneyFromObject(this object input)
    {
        try
        {
            if (input.ToString() != "0")
            {
                double zResult = double.Parse(input.ToString());

                string n = "";
                if (zResult % 1 == 0)
                {
                    n = "n0";
                }
                else
                {
                    n = "n2";
                }

                return zResult.ToString(n, CultureInfo.GetCultureInfo("vi-VN"));
            }
            else
            {
                return "0";
            }
        }
        catch (Exception)
        {
            return "Lỗi số";
        }
    }

    /// <summary>
    /// parse datetime from object to dd/MM/yyyy
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string ToDateString(this object obj)
    {
        try
        {
            string s = "";
            DateTime zDate = DateTime.Parse(obj.ToString());
            if (zDate != DateTime.MinValue)
            {
                s = zDate.ToString("dd/MM/yyyy");
            }

            return s;
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    public static DateTime ToDate(this object input)
    {
        string[] formats = {
                   "M/d/yyyy", "M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt",
                   "MM/dd/yyyy", "MM/dd/yyyy hh:mm:ss", "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm",
                   "M/d/yyyy h:mm:ss", "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt", "M/d/yyyy h:mm",
                    "dd/MM/yyyy hh:mm:ss","dd/MM/yyyy"};

        DateTime zResult;
        if (DateTime.TryParseExact(input.ToString(), formats,
                                     new CultureInfo("en-US"),
                                     DateTimeStyles.None,
                                     out zResult))
        {
            return zResult;
        }
        else
        {
            double tmp = 0;
            if (double.TryParse(input.ToString(), out tmp))
            {
                zResult = DateTime.FromOADate(tmp);
            }
        }

        return DateTime.MinValue;
    }
    public static bool ToBool(this object obj)
    {
        bool zResult = false;
        if (obj == null || obj.ToString() == "")
        {
            return false;
        }
        else
        {
            if (bool.TryParse(obj.ToString(), out zResult))
            {

            }
            return zResult;
        }
    }
    public static string ToAscii(this string unicode)
    {

        unicode = unicode.ToLower().Trim();
        unicode = Regex.Replace(unicode, "[áàảãạăắằẳẵặâấầẩẫậ]", "a");
        unicode = Regex.Replace(unicode, "[๖ۣۜ]", "");
        unicode = Regex.Replace(unicode, "[óòỏõọôồốổỗộơớờởỡợ]", "o");
        unicode = Regex.Replace(unicode, "[éèẻẽẹêếềểễệ]", "e");
        unicode = Regex.Replace(unicode, "[íìỉĩị]", "i");
        unicode = Regex.Replace(unicode, "[úùủũụưứừửữự]", "u");
        unicode = Regex.Replace(unicode, "[ýỳỷỹỵ]", "y");
        unicode = Regex.Replace(unicode, "[đ]", "d");
        unicode = unicode.Replace(" ", "-").Replace("[()]", "");
        unicode = Regex.Replace(unicode, "[-\\s+/]+", "-");
        unicode = Regex.Replace(unicode, "\\W+", "-"); //Nếu bạn muốn thay dấu khoảng trắng thành dấu "_" hoặc dấu cách " " thì thay kí tự bạn muốn vào đấu "-"
        return unicode;
    }
    public static string ToEnglish(string s)
    {
        string sspace = s.Replace(" ", "");
        string slow = sspace.ToLower();
        var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
        string temp = slow.Normalize(NormalizationForm.FormD);
        return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
    }

    public static string UrlEncode(this string input)
    {
        return System.Web.HttpUtility.UrlEncode(input);
    }
    public static string UrlDecode(this string input)
    {
        return System.Web.HttpUtility.UrlDecode(input);
    }

    /// <summary>
    /// Tính lấy năm
    /// </summary>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <returns></returns>
    public static int GetTotalYear(DateTime FromDate, DateTime ToDate)
    {
        DateTime zeroTime = new DateTime(1, 1, 1);

        DateTime a = FromDate;
        DateTime b = ToDate;

        TimeSpan span = b - a;
        // Because we start at year 1 for the Gregorian
        // calendar, we must subtract a year here.
        int years = (zeroTime + span).Year - 1;

        // 1, where my other algorithm resulted in 0.
        return years;
    }

    /// <summary>
    /// ước lượng thời gian như facebook
    /// </summary>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <returns></returns>
    public static string TimeAgo(DateTime FromDate, DateTime ToDate)
    {


        if (FromDate > ToDate)
        {
            //return "about sometime from now";
            return "vài ngày gần đây !.";
        }

        TimeSpan span = ToDate - FromDate;

        if (span.Days > 365)
        {
            int years = (span.Days / 365);
            if (span.Days % 365 != 0)
            {
                years += 1;
            }

            //return String.Format("about {0} {1} ago", years, years == 1 ? "year" : "years");
            return String.Format("khoản {0} {1} trước", years, years == 1 ? "năm" : "năm");
        }

        if (span.Days > 30)
        {
            int months = (span.Days / 30);
            if (span.Days % 31 != 0)
            {
                months += 1;
            }

            //return String.Format("about {0} {1} ago", months, months == 1 ? "month" : "months");
            return String.Format("khoản {0} {1} trước", months, months == 1 ? "tháng" : "tháng");
        }

        if (span.Days > 0)
        {
            //return String.Format("about {0} {1} ago", span.Days, span.Days == 1 ? "day" : "days");
            return String.Format("khoản {0} {1} trước", span.Days, span.Days == 1 ? "ngày" : "ngày");
        }

        if (span.Hours > 0)
        {
            //return String.Format("about {0} {1} ago", span.Hours, span.Hours == 1 ? "hour" : "hours");
            return String.Format("khoản {0} {1} trước", span.Hours, span.Hours == 1 ? "giờ" : "giờ");
        }

        if (span.Minutes > 0)
        {
            //return String.Format("about {0} {1} ago", span.Minutes, span.Minutes == 1 ? "minute" : "minutes");
            return String.Format("khoản {0} {1} trước", span.Minutes, span.Minutes == 1 ? "phút" : "phút");
        }

        if (span.Seconds > 5)
        {
            //return String.Format("about {0} seconds ago", span.Seconds);
            return String.Format("khoản {0} vài giây trước", span.Seconds);
        }

        if (span.Seconds <= 5)
        {
            return "mới gần đây !.";
        }

        return string.Empty;
    }
    public static string CalculateYourTime(DateTime FromDate, DateTime ToDate, out int YearNum)
    {

        int Years = new DateTime(ToDate.Subtract(FromDate).Ticks).Year - 1;
        DateTime _DOBDateNow = FromDate.AddYears(Years);
        int Months = 0;
        for (int i = 1; i <= 12; i++)
        {
            if (_DOBDateNow.AddMonths(i) == ToDate)
            {
                Months = i;
                break;
            }
            else if (_DOBDateNow.AddMonths(i) >= ToDate)
            {
                Months = i - 1;
                break;
            }
        }
        YearNum = Years;
        int Days = ToDate.Subtract(_DOBDateNow.AddMonths(Months)).Days;
        return "" + Years + " năm, " + Months + " tháng, " + Days + " ngày.";
        //return $"Age is {_Years} Years {_Months} Months {Days} Days";
    }

    public static string StatusCodes(string code)
    {
        switch (code)
        {
            case "100":
                return "Continue";

            case "101":
                return "Switching Protocols";

            case "102":
                return "Processing";

            case "103":
                return "Early Hints";

            case "200":
                return "OK";

            case "201":
                return "Created";

            case "400":
                return "Bad Request";

            case "401":
                return "Unauthorized";

            case "402":
                return "Payment Required";

            case "403":
                return "Forbidden";

            case "404":
                return "Not Found";

            case "500":
                return "Internal Server Error";

            case "501":
                return "Not Implemented";

            default:
                return string.Empty;
        }
    }

    /// <summary>
    /// Lấy chỉ số tiền USD mua bán của VIETCOMBANK
    /// </summary>
    /// <param name="CurrencyCode">USD</param>
    /// <returns></returns>
    public static string USDRate_VietComBank(string CurrencyCode)
    {
        try
        {
            XmlDocument xmlDocument = new XmlDocument();
            String xmlSourceUrl = "http://www.vietcombank.com.vn/ExchangeRates/ExrateXML.aspx";
            xmlDocument.Load(xmlSourceUrl);

            //từ đây hoàn toàn có thể thao tác dữ liệu xml bằng đối tượng xmlDocument
            //lấy ví zụ chuyển từ XmlDocument thành tập các đối tượng Generic dạng List<Exrate>
            XmlNodeList nodeList = xmlDocument.GetElementsByTagName("Exrate");
            List<Exrate> listExrate = null;
            if (nodeList != null && nodeList.Count > 0)
            {
                listExrate = new List<Exrate>();
                foreach (XmlNode xmlNode in nodeList)
                {
                    Exrate entityExrate = new Exrate();
                    entityExrate.CurrencyCode = xmlNode.Attributes["CurrencyCode"].InnerText;
                    entityExrate.CurrencyName = xmlNode.Attributes["CurrencyName"].InnerText;
                    entityExrate.Transfer = float.Parse(xmlNode.Attributes["Transfer"].InnerText);
                    entityExrate.Buy = float.Parse(xmlNode.Attributes["Buy"].InnerText);   //tỷ giá mua vào
                    entityExrate.Sell = float.Parse(xmlNode.Attributes["Sell"].InnerText);  //tỷ giá bán ra

                    listExrate.Add(entityExrate);
                }
            }

            return listExrate.Single(s => s.CurrencyCode == CurrencyCode).Sell.ToString();
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    /// <summary>
    /// tạo link tuyệt đối từ chuỗi url tương đối
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static string ToFullLink(this object obj)
    {
        string name = obj.ToString();
        string temp = Uri.EscapeUriString(name);
        string result = "";
        String strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
        String strUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
        result = strUrl + temp;
        return result;
    }

    /// <summary>
    /// Kiểm tra file đang sử dụng hoặc không
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    public static bool IsFileLocked(FileInfo file)
    {
        try
        {
            using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
            {
                stream.Close();
            }
        }
        catch (IOException)
        {
            //the file is unavailable because it is:
            //still being written to
            //or being processed by another thread
            //or does not exist (has already been processed)
            return true;
        }

        //file is not locked
        return false;
    }

    /// <summary>
    /// Tạo mã MD5
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string CreateMD5(string input)
    {
        // Use input string to calculate MD5 hash
        using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
        {
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }

    public static string HashPass(string input)
    {
        HashAlgorithm Hash = HashAlgorithm.Create("SHA1");
        byte[] pwordData = Encoding.Default.GetBytes(input);
        byte[] bHash = Hash.ComputeHash(pwordData);
        return Convert.ToBase64String(bHash);
    }

    public static Boolean VerifyHash(string NewPass, string OldPass)
    {
        string HashNewPass = HashPass(NewPass);
        return (OldPass == HashNewPass);
    }
    #endregion
}
public class Exrate
{
    private string _CurrencyCode = string.Empty;

    public string CurrencyCode
    {
        get { return _CurrencyCode; }
        set { _CurrencyCode = value; }
    }
    private string _CurrencyName = string.Empty;

    public string CurrencyName
    {
        get { return _CurrencyName; }
        set { _CurrencyName = value; }
    }
    private float _Buy = 0;

    public float Buy
    {
        get { return _Buy; }
        set { _Buy = value; }
    }
    private float _Transfer = 0;

    public float Transfer
    {
        get { return _Transfer; }
        set { _Transfer = value; }
    }
    private float _Sell = 0;

    public float Sell
    {
        get { return _Sell; }
        set { _Sell = value; }
    }
}

/// <summary>
/// ApiResult kết quả của giao tiếp client và server
/// </summary>
//public class ApiConnect
//{
//    public string Message
//    {
//        get;
//        set;
//    }

//    public int Status
//    {
//        get;
//        set;
//    }

//    public string Data
//    {
//        get;
//        set;
//    }

//    public ApiConnect()
//    {

//    }
//}
/// <summary>
/// ServerResult kết quả trả về của server
/// </summary>
public class ServerResult
{
    public string Message
    {
        get;
        set;
    } = "";
    public bool Success
    {
        get;
        set;
    } = false;
    public int Code
    {
        get;
        set;
    } = 0;

    public dynamic Data
    {
        get;
        set;
    } = "";

    public ServerResult()
    {

    }
}

//public class ApiSecurity
//{
//    public int Type
//    {
//        get;
//        set;
//    }
//    public string Username
//    {
//        get;
//        set;
//    }
//    public string Password
//    {
//        get;
//        set;
//    }
//    public string Token
//    {
//        get;
//        set;
//    }
//    public string PartnerNumber
//    {
//        get;
//        set;
//    }
//    public string Domain
//    {
//        get;
//        set;
//    }
//}
/// <summary>
/// Xử lý save hình ảnh
/// </summary>
public static class Imager
{
    /// <summary>  
    /// Save image as jpeg  
    /// </summary>  
    /// <param name="path">path where to save</param>  
    /// <param name="img">image to save</param>  
    public static void SaveJpeg(string path, System.Drawing.Image img)
    {
        var qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
        var jpegCodec = GetEncoderInfo("image/jpeg");

        var encoderParams = new EncoderParameters(1);
        encoderParams.Param[0] = qualityParam;
        img.Save(path, jpegCodec, encoderParams);
    }

    /// <summary>  
    /// Save image  
    /// </summary>  
    /// <param name="path">path where to save</param>  
    /// <param name="img">image to save</param>  
    /// <param name="imageCodecInfo">codec info</param>  
    public static void Save(string path, System.Drawing.Image img, ImageCodecInfo imageCodecInfo)
    {
        var qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);

        var encoderParams = new EncoderParameters(1);
        encoderParams.Param[0] = qualityParam;
        img.Save(path, imageCodecInfo, encoderParams);
    }

    /// <summary>  
    /// get codec info by mime type  
    /// </summary>  
    /// <param name="mimeType"></param>  
    /// <returns></returns>  
    public static ImageCodecInfo GetEncoderInfo(string mimeType)
    {
        return ImageCodecInfo.GetImageEncoders().FirstOrDefault(t => t.MimeType == mimeType);
    }

    /// <summary>  
    /// the image remains the same size, and it is placed in the middle of the new canvas  
    /// </summary>  
    /// <param name="image">image to put on canvas</param>  
    /// <param name="width">canvas width</param>  
    /// <param name="height">canvas height</param>  
    /// <param name="canvasColor">canvas color</param>  
    /// <returns></returns>  
    public static System.Drawing.Image PutOnCanvas(System.Drawing.Image image, int width, int height, Color canvasColor)
    {
        var res = new Bitmap(width, height);
        using (var g = Graphics.FromImage(res))
        {
            g.Clear(canvasColor);
            var x = (width - image.Width) / 2;
            var y = (height - image.Height) / 2;
            g.DrawImageUnscaled(image, x, y, image.Width, image.Height);
        }

        return res;
    }

    /// <summary>  
    /// the image remains the same size, and it is placed in the middle of the new canvas  
    /// </summary>  
    /// <param name="image">image to put on canvas</param>  
    /// <param name="width">canvas width</param>  
    /// <param name="height">canvas height</param>  
    /// <returns></returns>  
    public static System.Drawing.Image PutOnWhiteCanvas(System.Drawing.Image image, int width, int height)
    {
        return PutOnCanvas(image, width, height, Color.White);
    }

    /// <summary>  
    /// resize an image and maintain aspect ratio  
    /// </summary>  
    /// <param name="image">image to resize</param>  
    /// <param name="newWidth">desired width</param>  
    /// <param name="maxHeight">max height</param>  
    /// <param name="onlyResizeIfWider">if image width is smaller than newWidth use image width</param>  
    /// <returns>resized image</returns>  
    public static System.Drawing.Image Resize(System.Drawing.Image image, int newWidth, int maxHeight, bool onlyResizeIfWider)
    {
        if (onlyResizeIfWider && image.Width <= newWidth)
        {
            newWidth = image.Width;
        }

        var newHeight = image.Height * newWidth / image.Width;
        if (newHeight > maxHeight)
        {
            // Resize with height instead  
            newWidth = image.Width * maxHeight / image.Height;
            newHeight = maxHeight;
        }

        var res = new Bitmap(newWidth, newHeight);

        using (var graphic = Graphics.FromImage(res))
        {
            graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphic.SmoothingMode = SmoothingMode.HighQuality;
            graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphic.CompositingQuality = CompositingQuality.HighQuality;
            graphic.DrawImage(image, 0, 0, newWidth, newHeight);
        }

        return res;
    }

    /// <summary>  
    /// Crop an image   
    /// </summary>  
    /// <param name="img">image to crop</param>  
    /// <param name="cropArea">rectangle to crop</param>  
    /// <returns>resulting image</returns>  
    public static System.Drawing.Image Crop(System.Drawing.Image img, Rectangle cropArea)
    {
        var bmpImage = new Bitmap(img);
        var bmpCrop = bmpImage.Clone(cropArea, bmpImage.PixelFormat);
        return bmpCrop;
    }

    public static byte[] imageToByteArray(System.Drawing.Image imageIn)
    {
        MemoryStream ms = new MemoryStream();
        imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
        return ms.ToArray();
    }

    public static System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
    {
        MemoryStream ms = new MemoryStream(byteArrayIn);
        System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
        return returnImage;
    }

    //The actual converting function  
    public static string GetImage(object img)
    {
        return "data:image/jpg;base64," + Convert.ToBase64String((byte[])img);
    }


    public static void PerformImageResizeAndPutOnCanvas(string pFilePath, string pFileName, int pWidth, int pHeight, string pOutputFileName)
    {

        System.Drawing.Image imgBef;
        imgBef = System.Drawing.Image.FromFile(pFilePath + pFileName);


        System.Drawing.Image _imgR;
        _imgR = Imager.Resize(imgBef, pWidth, pHeight, true);


        System.Drawing.Image _img2;
        _img2 = Imager.PutOnCanvas(_imgR, pWidth, pHeight, System.Drawing.Color.White);

        //Save JPEG  
        Imager.SaveJpeg(pFilePath + pOutputFileName, _img2);

    }
}